
//          Copyright Seth Hendrick 2014.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file ../LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifndef ASM_JS

#include <fstream>
#include <sstream>

#include "UnitTest.h"

#include "Date.h"
#include "FileSystem.h"
#include "TestHelper.h"

std::string illegalDir;

TEST_GROUP(FileSystem)
{
    bool firstRun = true;

    TEST_SETUP()
    {
        //Date constructed automatically

        m_testOutput = testOutputPath + "/fileSystemTest/fileSystemTest" + m_date.getFullDateUnderscores();
        m_uut = new OS::FileSystem();
        if (firstRun){
            firstSetup();
            firstRun = false;
        }
    }
    TEST_TEARDOWN()
    {
        delete m_uut;
    }

    /**
     * \brief Test Fixture Setup
     */
    void firstSetup()
    {
        //First, create the directory we are going to be in
        m_uut->CreateDir(m_testOutput);
        CHECK(m_uut->DirExists(m_testOutput));
        #ifdef WIN32
            illegalDir = "con";
        #else
            illegalDir = "/lost+found/derp";
        #endif
    }

    std::string m_testOutput;
    OS::Date m_date;
    OS::FileSystem *m_uut;
};

/**
 * \brief creates a massive directory, with dirs, files, and other things
 */
void CreateMassiveDir(const OS::FileSystem &uut, const std::string &rootPath)
{
    uut.CreateDir(rootPath);
    CHECK(uut.DirExists(rootPath));

    uut.CreateDir(OS::FileSystem::PathJoin(rootPath, "dir1/dir1Child"));
    CHECK(uut.DirExists(OS::FileSystem::PathJoin(rootPath, "dir1")));
    CHECK(uut.DirExists(OS::FileSystem::PathJoin(rootPath, "dir1/dir1Child")));

    uut.CreateFile(OS::FileSystem::PathJoin(rootPath, "dir1/test1.txt"));
    CHECK(uut.FileExists(OS::FileSystem::PathJoin(rootPath, "dir1/test1.txt")));

    std::ofstream outFile1;
    outFile1.open("dir1/dir1Child/test1.txt");
    outFile1 << "Test File!\n\nYAY!";
    outFile1.close();

    uut.CreateDir(OS::FileSystem::PathJoin(rootPath, "dir2/dir2Child/dir2ChildChild"));
    CHECK(uut.DirExists(OS::FileSystem::PathJoin(rootPath, "dir2")));
    CHECK(uut.DirExists(OS::FileSystem::PathJoin(rootPath, "dir2/dir2Child")));
    CHECK(uut.DirExists(OS::FileSystem::PathJoin(rootPath, "dir2/dir2Child/dir2ChildChild")));

    uut.CreateFile(OS::FileSystem::PathJoin(rootPath, "dir2/dir2Child/test.txt"));
    CHECK(uut.FileExists(OS::FileSystem::PathJoin(rootPath, "dir2/dir2Child/test.txt")));

    std::ofstream outFile2;
    outFile2.open("dir2/dir2Child/dir2ChildChild/test1.txt");
    outFile2 << "Test File2!\n\nYAY!";
    outFile2.close();

    //Create files
    uut.CreateFile(OS::FileSystem::PathJoin(rootPath, "testFile1.txt"));
    CHECK(uut.FileExists(OS::FileSystem::PathJoin(rootPath, "testFile1.txt")));
}

TEST(FileSystem, GetInstanceTest)
{
    CHECK(&OS::FileSystem::GetInstance() == &OS::FileSystem::GetInstance());
}

/**
 * \brief tests the read file method during an open error
 */
TEST(FileSystem, ReadFileOpenFailure)
{
    // This file does not exist.
    const std::string badFile = "derp";
    try
    {
        m_uut->ReadFile(badFile);
        FAIL("Expected exception to throw when reading bad file.");
    }
    catch(const OS::FileSystemException &e)
    {
        CHECK_EQUAL(e.what(), OS::FileSystemException::ErrorPrefix + badFile);
        CHECK_EQUAL(e.GetBadFilePath(), badFile);
    }
    catch(...)
    {
        FAIL("Unknown exception");
    }

}
/**
 * \brief tests the read file method during a success
 */
TEST(FileSystem, ReadFileSuccess)
{
    const std::string fileLocation =
        OS::FileSystem::PathJoin(testFileDirectory, "testFile.txt");

    const std::string expectedFileContents = "test!"; // Whats in testFile.txt.

    try
    {
        const std::string actualFileContents = m_uut->ReadFile(fileLocation);
        CHECK_EQUAL(actualFileContents, expectedFileContents);
    }
    catch(...)
    {
        const std::string errorMessage = "Unexpected exception in ReadFileSucces().  Couldnt read from " + fileLocation;
        FAIL(errorMessage.c_str());
    }
}

/**
 * \brief tests the write file method during an open error
 */
TEST(FileSystem, WriteFileOpenFailure)
{
    // Put behind a directory that doesnt exist to force failure.
    const std::string badFile = "derp/herp";
    try
    {
        m_uut->WriteFile("This wont appear anywhere", badFile);
        FAIL("Expected excecption from WriteFileOpenFailure");
    }
    catch(const OS::FileSystemException &e)
    {
        CHECK_EQUAL(e.what(), OS::FileSystemException::ErrorPrefix + badFile);
        CHECK_EQUAL(e.GetBadFilePath(), badFile);
    }
    catch(...)
    {
        FAIL("WriteFileOpenFailure(): Unexpected exception");
    }
}

/**
 * \brief tests the write file during a write success
 */
TEST(FileSystem, WriteFileWriteSuccess)
{
    try
    {
        const std::string outputFile = OS::FileSystem::PathJoin(m_testOutput, "WriteTest");
        const std::string strToWrite = "Hello world!";

        m_uut->WriteFile(strToWrite, outputFile);

        const std::string actualString = m_uut->ReadFile(outputFile);
        CHECK_EQUAL(strToWrite, actualString);
    }
    catch(...)
    {
        FAIL("Expected exception in WriteFileWriteSuccess");
    }
}

/**
 * \brief tests the file creation method for failure
 */
TEST(FileSystem, CreateFileTestFaiure)
{
    // Put behind a directory that does not exist to force failure.
    const std::string badFile = "bad/file";
    try
    {
        m_uut->CreateFile(badFile);
        FAIL("Expected exception form CreateFileTestFailure");
    }
    catch (const OS::FileSystemException &e)
    {
        CHECK_EQUAL(e.what(), OS::FileSystemException::ErrorPrefix + badFile);
        CHECK_EQUAL(e.GetBadFilePath(), badFile);
    }
    catch(...)
    {
        FAIL("Unexpected exception from CreateFileTestFailure");
    }
}

/**
 *\brief tests the file creation method for success
 */
TEST(FileSystem, CreateFileTestSuccess)
{
    const std::string outputFile = OS::FileSystem::PathJoin(m_testOutput, "CreateFileTest.txt");
    try
    {
        m_uut->CreateFile(outputFile);

        CHECK(m_uut->FileExists(outputFile));
        CHECK(m_uut->IsFile(outputFile));
    }
    catch(...)
    {
        FAIL("Unexpected exception in CreateFileTestSuccess");
    }
}
/**
 * \brief tests the create dir method
 */
TEST(FileSystem, CreateDirTest)
{
    const std::string testDir1 = "createdDir1";
    const std::string testDir2 = "createdDir2";
    const std::string testDirChild1 = "createdDirChild1";
    const std::string testDirChild2 = "createdDirChild2";

    const std::string testDirLocation1 = OS::FileSystem::PathJoin(m_testOutput, testDir1);

    try
    {
        m_uut->CreateDir(testDirLocation1);
        CHECK(m_uut->DirExists(testDirLocation1));
        CHECK(m_uut->IsDir(testDirLocation1));

        //Test the empty string case
        try
        {
            m_uut->CreateDir("");
            FAIL("Expected invalid argument exception to be thrown.");
        }
        catch(const std::invalid_argument &e)
        {
            // Test Case PAssed
            CHECK(true);
        }

        //Test the case where we want to create a directory within a directory
        std::string testDirLocation2 = OS::FileSystem::PathJoin(m_testOutput, testDir2);
        testDirLocation2 = OS::FileSystem::PathJoin(testDirLocation2, testDirChild1);
        m_uut->CreateDir(testDirLocation2);
        CHECK(m_uut->DirExists(testDirLocation2));
        CHECK(m_uut->IsDir(testDirLocation2));

        //Test the case where we want to create a directory within an existing diretory
        std::string testDirLocation3 = OS::FileSystem::PathJoin(m_testOutput, testDir2);
        testDirLocation3 = OS::FileSystem::PathJoin(testDirLocation3, testDirChild2);
        m_uut->CreateDir(testDirLocation3);
        CHECK(m_uut->DirExists(testDirLocation3));
        CHECK(m_uut->IsDir(testDirLocation3));

        try
        {
            #ifdef WIN32
            m_uut->CreateDir(OS::FileSystem::PathJoin(m_testOutput, illegalDir));
            #else
            m_uut->CreateDir(illegalDir);
            #endif
        }
        catch(const OS::FileSystemException &e)
        {
            CHECK_EQUAL(std::string (e.what()), OS::FileSystemException::ErrorPrefix + illegalDir);
            CHECK_EQUAL(e.GetBadFilePath(), illegalDir);
        }
    }
    catch(...)
    {
        FAIL("Unexpected Exception in CreateDirTest");
    }
}

/**
 * \brief tests the case where the directory is an abspath from root
 */
TEST(FileSystem, CreateDirFromRootTest)
{
    const std::string testDir = "absDir";
    const std::string absPath = m_uut->GetCWD();
    std::string testDirLocation = OS::FileSystem::PathJoin(absPath, m_testOutput);
    testDirLocation = OS::FileSystem::PathJoin(testDirLocation, testDir);
    m_uut->CreateDir(testDirLocation);

    const std::string relTestDirLocation = OS::FileSystem::PathJoin(m_testOutput, testDir);
    CHECK(m_uut->DirExists(relTestDirLocation));
    CHECK(m_uut->IsDir(relTestDirLocation));
}


#ifndef MSVC //This test will pass on MSVC.  This is mainly to test linux systems.
/**
 * \brief tests the case where a directory tries to be made in root
 */
TEST(FileSystem, CreateDirInRootTest){
    //You should not be able to create a dir in root.
    const std::string testDir = "/sethderp";
    try
    {
        m_uut->CreateDir(testDir);
        FAIL("Expected exception in create dir in root test.");
    }
    catch (const OS::FileSystemException &e)
    {
        CHECK_EQUAL(std::string(e.what()), OS::FileSystemException::ErrorPrefix + testDir);
        CHECK_EQUAL(e.GetBadFilePath(), testDir);
    }
    CHECK(m_uut->DirExists(testDir) == false);
}
#endif

/**
 *\brief tests the case where the directory has two '//' in it
 */
TEST(FileSystem, CreateDirDoubleSlashes){
    const std::string testDir = "//twoSlashDir";
    std::string testDirLocation = OS::FileSystem::PathJoin(m_testOutput, testDir);
    testDirLocation = OS::FileSystem::PathJoin(testDirLocation, testDir);
    m_uut->CreateDir(testDirLocation);

    const std::string realTestDirLocation = OS::FileSystem::PathJoin(m_testOutput, "twoSlashDir");
    CHECK(m_uut->DirExists(realTestDirLocation));
    CHECK(m_uut->IsDir(realTestDirLocation));
}

/**
 * \brief tests the IsFile method
 */
TEST(FileSystem, IsFileTest){
    CHECK(m_uut->IsFile(m_testOutput) == false);
    CHECK(m_uut->IsFile(unEditableFilePath));

    const std::string badFile = "derp.txt";
    try
    {
        m_uut->IsFile(badFile);
        FAIL("Expected exception in IsFile test");
    }
    catch (const OS::FileSystemException &e)
    {
        CHECK_EQUAL(std::string(e.what()), OS::FileSystemException::ErrorPrefix + badFile);
        CHECK_EQUAL(e.GetBadFilePath(), badFile);
    }

    try
    {
        m_uut->IsFile("");
        FAIL("Expected exception in IsFile test");
    }
    catch (const OS::FileSystemException &e)
    {
        CHECK_EQUAL(std::string(e.what()), OS::FileSystemException::ErrorPrefix);
        CHECK_EQUAL(e.GetBadFilePath(), "");
    }
}

/**
 * \brief tests the isDir method
 */
TEST(FileSystem, IsDirTest){
    CHECK(m_uut->IsDir(m_testOutput));
    CHECK(m_uut->IsDir(unEditableFilePath) == false);

    const std::string badDir = "derp";
    try
    {
        m_uut->IsDir(badDir);
        FAIL("Expected exception in IsDir test");

    }
    catch (const OS::FileSystemException &e)
    {
        CHECK_EQUAL(std::string(e.what()), OS::FileSystemException::ErrorPrefix + badDir);
        CHECK_EQUAL(e.GetBadFilePath(), badDir);
    }

    try
    {
        m_uut->IsDir("");
        FAIL("Expected exception in IsDir test");
    }
    catch (const OS::FileSystemException &e)
    {
        CHECK_EQUAL(std::string(e.what()), OS::FileSystemException::ErrorPrefix);
        CHECK_EQUAL(e.GetBadFilePath(), "");
    }
}

/**
 * \brief tests the file exists method
 */
TEST(FileSystem, FileExistsTest)
{
    CHECK(m_uut->FileExists(unEditableFilePath));
    CHECK(m_uut->FileExists("derp.txt") == false);
}

/**
 * \brief tests the directory exists method
 */
TEST(FileSystem, DirExistsTest)
{
    CHECK(m_uut->DirExists(m_testOutput));
    CHECK(m_uut->DirExists(unEditableFilePath) == false);
    CHECK(m_uut->DirExists("derp") == false);
}

/*
///\brief tests the copyFile method
TEST(FileSystem, copyFileTest){
    std::string originalFile = "originalCopy.txt";
    std::string newFile = "copiedFile.txt";
    std::stringstream originalSS;
    std::stringstream copySS;
    originalSS << fileTestOutputPath << "/" << originalFile;
    copySS << fileTestOutputPath << "/" << newFile;

    //Make arbitrary original file
    std::ofstream outFile(originalSS.str());
    outFile << "Hello, this is a file that needs to be copied\n\n\nPretty cool right?";
    outFile.close();

    //Test same file name
    CHECK(m_uut->copyFile(originalSS.str(), originalSS.str()));
    CHECK(m_uut->fileExists(originalSS.str()));
    CHECK(!m_uut->fileExists(copySS.str()));

    CHECK(m_uut->copyFile(originalSS.str(), copySS.str()));
    CHECK(m_uut->fileExists(originalSS.str()));
    CHECK(m_uut->fileExists(copySS.str()));

    //Test file doesn't exist case
    CHECK(!m_uut->copyFile("derp.txt", copySS.str()));

    //Test two empty files
    std::string blankFile1 = OS::FileSystem::pathJoin(fileTestOutputPath, "blankFile1");
    std::string blankFile2 = OS::FileSystem::pathJoin(fileTestOutputPath, "blankFile2");

    CHECK(m_uut->createFile(blankFile1));
    CHECK(m_uut->fileExists(blankFile1));
    CHECK(m_uut->createFile(blankFile2));
    CHECK(m_uut->fileExists(blankFile2));

    CHECK(m_uut->copyFile(blankFile1, blankFile2));
}

///\brief tests the copyDir method
TEST(FileSystem, copyDirTest){
    std::string testDir = OS::FileSystem::pathJoin(fileTestOutputPath, "copyDir1");
    std::string copiedDir = OS::FileSystem::pathJoin(fileTestOutputPath, "copyDir2");
    createMassiveDir(m_uut, testDir);
    CHECK(m_uut->copyDir(testDir, copiedDir));
    CHECK_EQUAL(m_uut->compareDirs(testDir, copiedDir), OS::FileSystem::FILE_EQUAL);

    //Test the case where the dirs have the same name
    CHECK(!m_uut->copyDir(testDir, testDir));
    CHECK(m_uut->dirExists(testDir));

    //Test the case where a directory does not exist
    CHECK(!m_uut->copyDir("derp", testDir));
    CHECK(!m_uut->copyDir("derp", "herp"));

    //Tests the case where we are copying to an illegal directory
    #ifdef WIN32
    CHECK(!m_uut->copyDir(testDir, OS::FileSystem::pathJoin(fileTestOutputPath, illegalDir)));
	#else
	CHECK(!m_uut->copyDir(testDir, illegalDir));
	#endif

    //Test the case where listFiles fail
    m_uut->m_failListFilesInDir = true;
    copiedDir = OS::FileSystem::pathJoin(fileTestOutputPath, "copyDir3");
    CHECK(!m_uut->copyDir(testDir, copiedDir));
}

///\brief tests the renameFile method
TEST(FileSystem, renameFileTest){
    std::string orginalFile = "toBeRenamed.txt";
    std::stringstream orginalSS;
    orginalSS << fileTestOutputPath << "/" << orginalFile;

    std::string newFile = "renamedFile.txt";
    std::stringstream newSS;
    newSS << fileTestOutputPath << "/" << newFile;

    //Ensure the orginal file was created
    CHECK(m_uut->createFile(orginalSS.str()));
    CHECK(m_uut->fileExists(orginalSS.str()));
    CHECK(!m_uut->fileExists(newSS.str()));

    //Ensure when a file does not exist is send in, it is not created
    CHECK(!m_uut->renameFile("derp.txt", newSS.str()));
    CHECK(!m_uut->fileExists(newSS.str()));

    //Ensure the no op situation
    CHECK(m_uut->renameFile(orginalSS.str(), orginalSS.str()));
    CHECK(m_uut->fileExists(orginalSS.str()));
    CHECK(!m_uut->fileExists(newSS.str()));

    //Ensure rename is successful by the old file not existing, and the new one existing
    CHECK(m_uut->renameFile(orginalSS.str(), newSS.str()));
    CHECK(!m_uut->fileExists(orginalSS.str()));
    CHECK(m_uut->fileExists(newSS.str()));
}

///\brief tests the renameDir method
TEST(FileSystem, renameDirTest){
    std::string orginalDir = "dirToBeRenamed";
    std::string orginalPath;
    orginalPath = fileTestOutputPath + "/" + orginalDir;

    std::string newDir = "renamedDir";
    std::string newPath;
    newPath = fileTestOutputPath + "/" + newDir;

    //Ensure the orginal file was created
    CHECK(m_uut->createDir(orginalPath));
    CHECK(m_uut->dirExists(orginalPath));
    CHECK(!m_uut->dirExists(newPath));

    //Ensure when a file does not exist is send in, it is not created
    CHECK(!m_uut->renameDir("derp", newPath));
    CHECK(!m_uut->dirExists(newPath));

    //Ensure the no op situation
    CHECK(m_uut->renameDir(orginalPath, orginalPath));
    CHECK(m_uut->dirExists(orginalPath));
    CHECK(!m_uut->dirExists(newPath));

    //Ensure rename is successful by the old file not existing, and the new one existing
    CHECK(m_uut->renameDir(orginalPath, newPath));
    CHECK(!m_uut->dirExists(orginalPath));
    CHECK(m_uut->dirExists(newPath));
}

///\brief tests the movefile ability of renameFile
TEST(FileSystem, moveFileTest){
    std::string originalFile = "toBeMoved.txt";
    std::string originalDir = "moveFileTest1";
    std::string newFile = "movedFile.txt";
    std::string newDir = "moveFileTest2";

    std::stringstream originalDirSS;
    originalDirSS << fileTestOutputPath << "/" << originalDir;

    std::stringstream originalFileSS;
    originalFileSS << originalDirSS.str() << "/" << originalFile;

    std::stringstream newDirSS;
    newDirSS << fileTestOutputPath << "/" << newDir;

    std::stringstream newFileSS;
    newFileSS << newDirSS.str() << "/" << newFile;

    //Create the needed directories and files
    CHECK(m_uut->createDir(originalDirSS.str()));
    CHECK(m_uut->dirExists(originalDirSS.str()));
    CHECK(m_uut->createFile(originalFileSS.str()));
    CHECK(m_uut->fileExists(originalFileSS.str()));
    CHECK(m_uut->createDir(newDirSS.str()));
    CHECK(m_uut->dirExists(newDirSS.str()));

    //Test moving
    CHECK(m_uut->renameFile(originalFileSS.str(), newFileSS.str()));

    //Ensure the file was moved
    CHECK(!m_uut->fileExists(originalFileSS.str()));
    CHECK(m_uut->fileExists(newFileSS.str()));
}

///\brief tests the move dir ability of renameDir
TEST(FileSystem, moveDirTest){
    std::string originalDir = "toBeMoved";
    std::string originalDirLocation = "moveFileTest1";
    std::string newDir = "movedDir";
    std::string newDirLocation = "moveFileTest2";

    std::stringstream originalDirLocationSS;
    originalDirLocationSS << fileTestOutputPath << "/" << originalDirLocation;

    std::stringstream originalDirSS;
    originalDirSS << originalDirLocationSS.str() << "/" << originalDir;

    std::stringstream newDirLocationSS;
    newDirLocationSS << fileTestOutputPath << "/" << newDirLocation;

    std::stringstream newDirSS;
    newDirSS << newDirLocationSS.str() << "/" << newDir;

    //Create the needed directories and files
    CHECK(m_uut->createDir(originalDirLocationSS.str()));
    CHECK(m_uut->dirExists(originalDirLocationSS.str()));
    CHECK(m_uut->createDir(originalDirSS.str()));
    CHECK(m_uut->dirExists(originalDirSS.str()));
    CHECK(m_uut->createDir(newDirLocationSS.str()));
    CHECK(m_uut->dirExists(newDirLocationSS.str()));

    //Test moving
    CHECK(m_uut->renameDir(originalDirSS.str(), newDirSS.str()));

    //Ensure the file was moved
    CHECK(!m_uut->dirExists(originalDirSS.str()));
    CHECK(m_uut->dirExists(newDirSS.str()));
}

///\brief tests the delete file method
TEST(FileSystem, removeFileTestFailure){
    addMock();
    EXPECT_CALL(*m_cstdio, remove(m_file))
        .WillOnce(testing::Return(1));
    CHECK(!m_uut->deleteFile(m_file));
}

TEST(FileSystem, removeFileTestSuccess){
    addMock();
    EXPECT_CALL(*m_cstdio, remove(m_file))
        .WillOnce(testing::Return(0));
    CHECK(m_uut->deleteFile(m_file));
}

///\brief tests the delete dir method
TEST(FileSystem, removeDirTest){
    std::string testDir = "toBeDeletedDir";
    std::string dir;
    dir = fileTestOutputPath + "/" + testDir;
    CHECK(m_uut->createDir(dir));
    CHECK(m_uut->dirExists(dir));
    CHECK(m_uut->deleteDir(dir));
    CHECK(!m_uut->dirExists(dir));

    CHECK(!m_uut->deleteDir("derp"));

    //Check for a massive dir
    std::string massiveDir = OS::FileSystem::pathJoin(fileTestOutputPath,"massiveToBeDeleted");
    createMassiveDir(m_uut, massiveDir);
    CHECK(m_uut->deleteDir(massiveDir));
    CHECK(!m_uut->dirExists(massiveDir));
}

///brief tests the list files in dir method
TEST(FileSystem, listFilesInDirTest){
    std::string testFile1 = "testFile1.txt";
    std::string testFile2 = "testFile2.txt";
    std::string testFile3 = "testFile3.txt";
    std::string testDirFile = "testDir";
    std::string testDir = "listDirTest";

    std::string uutDir = OS::FileSystem::pathJoin(fileTestOutputPath, testDir);
    CHECK(m_uut->createDir(uutDir));
    CHECK(m_uut->createDir(OS::FileSystem::pathJoin(uutDir, testDirFile)));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(uutDir, testFile1)));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(uutDir, testFile2)));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(uutDir, testFile3)));

    std::deque <std::string> files;
    CHECK(m_uut->listFilesInDir(uutDir, files));
    CHECK_EQUAL(files.size(), 6);  //6 due to ., .., and the three files
}

///\brief tests the compare Files method
TEST(FileSystem, compareFilesTest){
    std::string testFile1 = "sameFile1.txt";
    std::string testFile2 = "sameFile2.txt";
    std::string testFile3 = "differentFile.txt";
    std::string testDir = "compareFileTest";

    std::string uutDir = OS::FileSystem::pathJoin(fileTestOutputPath, testDir);
    CHECK(m_uut->createDir(uutDir));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(uutDir, testFile1)));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(uutDir, testFile2)));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(uutDir, testFile3)));

    std::string sameString = "Hello, this string is between two files\n\ncool huh?";
    std::string differentString = "Different String!";

    std::ofstream sameFile1;
    std::ofstream sameFile2;
    std::ofstream differentFile;
    sameFile1.open(OS::FileSystem::pathJoin(uutDir, testFile1));
    sameFile1 << sameString;
    sameFile1.close();

    sameFile2.open(OS::FileSystem::pathJoin(uutDir, testFile2));
    sameFile2 << sameString;
    sameFile2.close();

    differentFile.open(OS::FileSystem::pathJoin(uutDir, testFile3));
    differentFile << differentString;
    differentFile.close();

    CHECK_EQUAL(m_uut->compareFiles(OS::FileSystem::pathJoin(uutDir, testFile1), OS::FileSystem::pathJoin(uutDir, testFile2)), OS::FileSystem::FILE_EQUAL);

    CHECK_EQUAL(m_uut->compareFiles(OS::FileSystem::pathJoin(uutDir, testFile1), OS::FileSystem::pathJoin(uutDir, testFile3)), OS::FileSystem::FILE_NOT_EQUAL);

    CHECK_EQUAL(m_uut->compareFiles(OS::FileSystem::pathJoin(uutDir, testFile1),OS::FileSystem::pathJoin(uutDir, testFile1)), OS::FileSystem::FILE_EQUAL);

    //Test the bad param cases
    CHECK_EQUAL(m_uut->compareFiles(testFileDirectory, OS::FileSystem::pathJoin(uutDir, testFile1)), OS::FileSystem::FILE_ERROR);
    CHECK_EQUAL(m_uut->compareFiles(OS::FileSystem::pathJoin(uutDir, testFile1), testFileDirectory), OS::FileSystem::FILE_ERROR);
    CHECK_EQUAL(m_uut->compareFiles(testFileDirectory, testFileDirectory), OS::FileSystem::FILE_ERROR);
    CHECK_EQUAL(m_uut->compareFiles("DERP.txt", OS::FileSystem::pathJoin(uutDir, testFile3)), OS::FileSystem::FILE_ERROR);
    CHECK_EQUAL(m_uut->compareFiles(OS::FileSystem::pathJoin(uutDir, testFile1), "HERP.txt"), OS::FileSystem::FILE_ERROR);
    CHECK_EQUAL(m_uut->compareFiles("DERP.txt", "HERP.txt"), OS::FileSystem::FILE_ERROR);
}

///\brief tests the compare directories method
TEST(FileSystem, compareDirsTest){
    std::string testDir = OS::FileSystem::pathJoin(fileTestOutputPath,"compareDirTest");
    //Setup
    CHECK(m_uut->createDir(testDir));
    CHECK(m_uut->dirExists(testDir));

    //Test the case where both dirs have the same path
    CHECK_EQUAL(m_uut->compareDirs(testDir, testDir), OS::FileSystem::FILE_EQUAL);
    CHECK(m_uut->dirExists(testDir));

    //Test the case where neither dirs exist
    CHECK_EQUAL(m_uut->compareDirs("derp", "derp"), OS::FileSystem::FILE_ERROR);
    //Test the case where both are files
    CHECK_EQUAL(m_uut->compareDirs(unEditableFilePath, unEditableFilePath), OS::FileSystem::FILE_ERROR);
    //Test the case where ones a dir and one is not
    CHECK_EQUAL(m_uut->compareDirs(testDir, unEditableFilePath), OS::FileSystem::FILE_ERROR);
    CHECK_EQUAL(m_uut->compareDirs(unEditableFilePath, testDir), OS::FileSystem::FILE_ERROR);
    //Test the care where ones a dir, and one does not exist
    CHECK_EQUAL(m_uut->compareDirs(testDir, "derp"), OS::FileSystem::FILE_ERROR);
    CHECK_EQUAL(m_uut->compareDirs("derp", testDir), OS::FileSystem::FILE_ERROR);

    //Set up two dirs with different number of files in each
    std::string differentSizeDir = OS::FileSystem::pathJoin(testDir, "differentSizeDirTest");
    CHECK(m_uut->createDir(differentSizeDir));
    CHECK(m_uut->dirExists(differentSizeDir));

    std::string differentSizeDir1 = OS::FileSystem::pathJoin(differentSizeDir, "differentSizeDir1");
    CHECK(m_uut->createDir(differentSizeDir1));
    CHECK(m_uut->dirExists(differentSizeDir1));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(differentSizeDir1, "test1.txt")));
    CHECK(m_uut->fileExists(OS::FileSystem::pathJoin(differentSizeDir1, "test1.txt")));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(differentSizeDir1, "test2.txt")));
    CHECK(m_uut->fileExists(OS::FileSystem::pathJoin(differentSizeDir1, "test2.txt")));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(differentSizeDir1, "test3.txt")));
    CHECK(m_uut->fileExists(OS::FileSystem::pathJoin(differentSizeDir1, "test3.txt")));

    std::string differentSizeDir2 = OS::FileSystem::pathJoin(differentSizeDir, "differentSizeDir2");
    CHECK(m_uut->createDir(differentSizeDir2));
    CHECK(m_uut->dirExists(differentSizeDir2));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(differentSizeDir2, "test1.txt")));
    CHECK(m_uut->fileExists(OS::FileSystem::pathJoin(differentSizeDir2, "test1.txt")));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(differentSizeDir2, "test2.txt")));
    CHECK(m_uut->fileExists(OS::FileSystem::pathJoin(differentSizeDir2, "test2.txt")));

    CHECK_EQUAL(m_uut->compareDirs(differentSizeDir1, differentSizeDir2), OS::FileSystem::FILE_NOT_EQUAL);

    //Set up two directories with miss matching names
    std::string differentFileNameDir = OS::FileSystem::pathJoin(testDir, "differentFileNames");
    CHECK(m_uut->createDir(differentFileNameDir));
    CHECK(m_uut->dirExists(differentFileNameDir));

    std::string differentFileNameDir1 = OS::FileSystem::pathJoin(differentFileNameDir, "differentFileNameDir1");
    CHECK(m_uut->createDir(differentFileNameDir1));
    CHECK(m_uut->dirExists(differentFileNameDir1));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(differentFileNameDir1, "test1.txt")));
    CHECK(m_uut->fileExists(OS::FileSystem::pathJoin(differentFileNameDir1, "test1.txt")));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(differentFileNameDir1, "LOL.txt")));
    CHECK(m_uut->fileExists(OS::FileSystem::pathJoin(differentFileNameDir1, "LOL.txt")));

    std::string differentFileNameDir2 = OS::FileSystem::pathJoin(differentFileNameDir, "differentFileNameDir2");
    CHECK(m_uut->createDir(differentFileNameDir2));
    CHECK(m_uut->dirExists(differentFileNameDir2));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(differentFileNameDir2, "test1.txt")));
    CHECK(m_uut->fileExists(OS::FileSystem::pathJoin(differentFileNameDir2, "test1.txt")));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(differentFileNameDir2, "test2.txt")));
    CHECK(m_uut->fileExists(OS::FileSystem::pathJoin(differentFileNameDir2, "test2.txt")));

    CHECK_EQUAL(m_uut->compareDirs(differentFileNameDir1, differentFileNameDir2), OS::FileSystem::FILE_NOT_EQUAL);

    //Set up two directories with miss matching types, but same names
    std::string differentFileTypesDir = OS::FileSystem::pathJoin(testDir, "differentFileTypesDir");
    CHECK(m_uut->createDir(differentFileTypesDir));
    CHECK(m_uut->dirExists(differentFileTypesDir));

    std::string differentFileTypesDir1 = OS::FileSystem::pathJoin(differentFileTypesDir, "differentFileTypesDir1");
    CHECK(m_uut->createDir(differentFileTypesDir1));
    CHECK(m_uut->dirExists(differentFileTypesDir1));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(differentFileTypesDir1, "test1.txt")));
    CHECK(m_uut->fileExists(OS::FileSystem::pathJoin(differentFileTypesDir1, "test1.txt")));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(differentFileTypesDir1, "test2")));
    CHECK(m_uut->fileExists(OS::FileSystem::pathJoin(differentFileTypesDir1, "test2")));

    std::string differentFileTypesDir2 = OS::FileSystem::pathJoin(differentFileTypesDir, "differentFileTypesDir2");
    CHECK(m_uut->createDir(differentFileTypesDir2));
    CHECK(m_uut->dirExists(differentFileTypesDir2));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(differentFileTypesDir2, "test1.txt")));
    CHECK(m_uut->fileExists(OS::FileSystem::pathJoin(differentFileTypesDir2, "test1.txt")));
    CHECK(m_uut->createDir(OS::FileSystem::pathJoin(differentFileTypesDir2, "test2")));
    CHECK(m_uut->dirExists(OS::FileSystem::pathJoin(differentFileTypesDir2, "test2")));

    CHECK_EQUAL(m_uut->compareDirs(differentFileTypesDir1, differentFileTypesDir2), OS::FileSystem::FILE_NOT_EQUAL);

    //Set up two directories with just files
    std::string sameDirWithJustFiles = OS::FileSystem::pathJoin(testDir, "sameDirWithJustFiles");
    CHECK(m_uut->createDir(sameDirWithJustFiles));
    CHECK(m_uut->dirExists(sameDirWithJustFiles));

    std::string sameDirWithJustFiles1 = OS::FileSystem::pathJoin(sameDirWithJustFiles, "sameDirWithJustFiles1");
    CHECK(m_uut->createDir(sameDirWithJustFiles1));
    CHECK(m_uut->dirExists(sameDirWithJustFiles1));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(sameDirWithJustFiles1, "test1.txt")));
    CHECK(m_uut->fileExists(OS::FileSystem::pathJoin(sameDirWithJustFiles1, "test1.txt")));
    std::ofstream outFile1;
    outFile1.open((OS::FileSystem::pathJoin(sameDirWithJustFiles1, "test2.txt")));
    outFile1 << sameDirWithJustFiles << "\n\n" << sameDirWithJustFiles;
    outFile1.close();

    std::string sameDirWithJustFiles2 = OS::FileSystem::pathJoin(sameDirWithJustFiles, "sameDirWithJustFiles2");
    CHECK(m_uut->createDir(sameDirWithJustFiles2));
    CHECK(m_uut->dirExists(sameDirWithJustFiles2));
    CHECK(m_uut->createFile(OS::FileSystem::pathJoin(sameDirWithJustFiles2, "test1.txt")));
    CHECK(m_uut->fileExists(OS::FileSystem::pathJoin(sameDirWithJustFiles2, "test1.txt")));
    std::ofstream outFile2;
    outFile2.open((OS::FileSystem::pathJoin(sameDirWithJustFiles2, "test2.txt")));
    outFile2 << sameDirWithJustFiles << "\n\n" << sameDirWithJustFiles;
    outFile2.close();

    CHECK_EQUAL(m_uut->compareDirs(sameDirWithJustFiles1, sameDirWithJustFiles2), OS::FileSystem::FILE_EQUAL);

    //Set up two directories files and dirs
    //Tested in copyDirTest

    //Test the case where directory listing fails
    m_uut->m_failListFilesInDir = true;
    CHECK_EQUAL(m_uut->compareDirs(sameDirWithJustFiles1, sameDirWithJustFiles2), OS::FileSystem::FILE_ERROR);
}

*/

/**
 * \brief Tests the Path Join method.
 */
TEST(FileSystem, PathJoinTest){
    std::string parent = "parent";
    std::string child = "child";
    std::stringstream ss;
    ss << parent << "/" << child;

    CHECK_EQUAL(OS::FileSystem::PathJoin(parent, child), ss.str());
}

#endif
