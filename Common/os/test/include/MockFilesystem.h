
//          Copyright Seth Hendrick 2014.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file ../../LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifndef MOCKFILESYSTEM_H_INCLUDED
#define MOCKFILESYSTEM_H_INCLUDED

#include <vector>
#include <string>

#include "gmock/gmock.h"

#include "IFileSystem.h"

namespace OS
{

class MockFileSystem : public IFileSystem
{
    public:
        virtual ~MockFileSystem(){};

        MOCK_CONST_METHOD0(GetCWD, const std::string());
        MOCK_CONST_METHOD2(ReadFile, const std::string (const std::string &fileName));
        MOCK_CONST_METHOD2(WriteFile, void(const std::string &stringToWrite, const std::string &fileName));
        MOCK_CONST_METHOD1(CreateFile, void(const std::string &filePath));
        MOCK_CONST_METHOD1(CreateDir, void(const std::string &dirPath));
        MOCK_CONST_METHOD1(FileExists, const bool(const std::string &filePath));
        MOCK_CONST_METHOD1(DirExists, const bool(const std::string &dirPath));
        MOCK_CONST_METHOD1(IsFile, const bool(const std::string &filePath));
        MOCK_CONST_METHOD1(IsDir, const bool(const std::string &dirPath));
        MOCK_CONST_METHOD2(CopyFile, void(const std::string &originalFile, const std::string &destinationPath));
        MOCK_CONST_METHOD2(CopyDir, void(const std::string &originalDir, const std::string &destinationPath));
        MOCK_CONST_METHOD2(RenameFile, void(const std::string &originalFile, const std::string &newFileName));
        MOCK_CONST_METHOD2(RenameDir, void(const std::string &originalDir, const std::string &newDirName));
        MOCK_CONST_METHOD1(DeleteFile, void(const std::string &filePath));
        MOCK_CONST_METHOD1(DeleteDir, void(const std::string &dirPath));
        MOCK_CONST_METHOD2(GetFilesInDir, const std::vector<std::string>(const std::string &dirPath);
        MOCK_CONST_METHOD2(CompareFiles, const bool(const std::string &fileOne, const std::string &fileTwo));
        MOCK_CONST_METHOD2(CompareDirs, const bool(const std::string &dirOne, const std::string &dirTwo));
};

}

#endif // MOCKFILESYSTEM_H_INCLUDED
