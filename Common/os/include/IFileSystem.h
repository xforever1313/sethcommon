
//          Copyright Seth Hendrick 2014.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file ../LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifndef FILESYSTEMINTERFACE_H_INCLUDED
#define FILESYSTEMINTERFACE_H_INCLUDED

#include <vector>
#include <string>

namespace OS
{

class IFileSystem
{
    public:
        virtual ~IFileSystem(){}

        virtual const std::string GetCWD() const = 0;
        virtual const std::string ReadFile(const std::string &filePath) const = 0;
        virtual void WriteFile(const std::string &stringToWrite, const std::string &filePath) const = 0;
        virtual void CreateFile(const std::string &filePath) const = 0;
        virtual void CreateDir(const std::string &dirPath) const = 0;
        virtual const bool FileExists(const std::string &filePath) const = 0;
        virtual const bool DirExists(const std::string &dirPath) const = 0;
        virtual const bool IsFile(const std::string &filePath) const = 0;
        virtual const bool IsDir(const std::string &dirPath) const = 0;
        virtual void CopyFile(const std::string &originalFile, const std::string &destinationPath) const = 0;
        virtual void CopyDir(const std::string &originalDir, const std::string &destinationPath) const = 0;
        virtual void RenameFile(const std::string &originalFile, const std::string &newFileName) const = 0;
        virtual void RenameDir(const std::string &originalDir, const std::string &newDirName) const = 0;
        virtual void DeleteFile(const std::string &filePath) const = 0;
        virtual void DeleteDir (const std::string &dirPath) const = 0;
        virtual const std::vector<std::string> GetFilesInDir(const std::string &dirPath) const = 0;
        virtual const bool CompareFiles (const std::string &fileOne, const std::string &fileTwo) const = 0;
        virtual const bool CompareDirs (const std::string &dirOne, const std::string &dirTwo) const = 0;
};

}

#endif // FILESYSTEMINTERFACE_H_INCLUDED
