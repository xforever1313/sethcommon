
//          Copyright Seth Hendrick 2014.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file ../LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifndef FILE_SYSTEM_H_INCLUDED
#define FILE_SYSTEM_H_INCLUDED

#ifdef ASM_JS
    #pragma message("FileSystem not fully supported nor tested with emscripten. Some functions may not work like with C++. Use at own risk.  Good luck!")
#endif

#include <exception>
#include <string>
#include <vector>

#include "IFileSystem.h"

namespace OS{

/**
 * \brief Thrown when a file or directory can not be opened.
 */
class FileSystemException : public std::exception
{
    public:
        /**
         * \brief Constructor
         * \param filePath - The file path that could not be found.
         */
        explicit FileSystemException (const std::string &filePath);

        /**
         * \brief Destructor
         */
        ~FileSystemException();

        /**
         * \brief Get the error message.
         * \return The error message.
         */
        const char *what() const noexcept override;

        /**
         * \brief Get the file path that was bad.
         * \return the file path that was bad.
         */
        const std::string &GetBadFilePath() const;

    private:
        static const std::string ErrorPrefix;

        const std::string m_errorMessage;
        const std::string m_badFilePath;
};

/**
* \class FileSystem
* \brief This will be our interface to the file system. It wraps all unistd
*        functionality we'll need
* \warning Shortcuts, SymLinks, and hardlinks result in undefined behavior
* \warning FileSystem is not fully supported nor has been tested with emscripten.
*          Some functions may not work like with C++.  Use at own risk.  Good Luck!
*
* \warning It is assumed all path separators passed into parameters are '/', if not change it
* \author Seth Hendrick
*/
class FileSystem : public IFileSystem
{
    public:
        static const std::string THIS_DIR; ///<Name for the current directory (usually ".")
        static const std::string UP_DIR;  ///<Name for the directory above the current one (usually "..")
        static const char FILE_SEPARATOR; ///<What separates directories and files, '/'.

        ~FileSystem();

        /**
         * \brief Returns the instance.
         */
        static const FileSystem &GetInstance();

        /**
         * \brief Combines the given child path name to the parent, resulting in
         *        parentFilePath/childFilePath.
         * \param parent - What comes before the file separator, '/'
         * \param chile - What comes after the file separator, '/'
         * \return "parent/child"
         */
        static const std::string PathJoin(const std::string &parent, const std::string &child);

        /**
         * \brief Gets the current working directory.
         */
        const std::string GetCWD() const override;

        /**
         * \brief Reads the file, and returns its contents.
         * \param filePath - The path to read from.
         * \throw FileSystemException if the file can not be opened.
         * \return The contents in the file.
         */
        const std::string ReadFile(const std::string &filePath) const override;

        /**
         * \brief Writes the given string to the given file name.  This OVERWRITES the file.
         * \param stringToWrite - What gets written to the file.
         * \param filePath - The path to write to.
         * \warning If a write error occurs, the original file contents is lost.
         * \throw FileSystemException if file can not be opened for writing.
         */
        void WriteFile(const std::string &stringToWrite, const std::string &filePath) const override;

        /**
         * \brief Creates a blank file.
         * \param filePath - The path of the file to create.
         * \throw FileSystemException if file can not be opened for writing.
         */
        void CreateFile(const std::string &filePath) const override;

        /**
         * \brief creates a directory.
         *        Can also create them recusively (e.g. passing in "dir1/dir2" will create dir2 inside of dir1)
         * \param dirPath - the path to the directory to make.
         * \warning It is assumed all path separators in dirPath are '/', if not change it
         * \throw FileSystemException if directory can not be created.
         */
        void CreateDir(const std::string &dirPath) const override;

        /**
         * \brief Checks to see if the given file exists or not.
         * \param filePath - The path to check
         * \warning Undefined behavior if a directory is passed in.
         * \return True if the file exists, false otherwise.
         */
        const bool FileExists(const std::string &filePath) const override;

        /**
         * \brief Checks to see if the given directory exists or not.
         * \param dirPath - The path to check
         * \return True if the directory exists, false otherwise.
         */
        const bool DirExists(const std::string &dirPath) const override;

        /**
         * \brief Checks to see if the given path is a file.
         * \param filePath - the path to check
         * \warning This is not the method to use if checking to see if a file
         *          exists.  Use FileExists for that.
         * \return True if the path is a file, false if its a directory or does not exist.
         */
        const bool IsFile(const std::string &filePath) const override;

        /**
         * \brief Checks to see if the given path is a directory.
         * \param dirPath - the path to check
         * \warning This is not the method to use if checking to see if a directory
         *          exists.  Use DirExists for that.
         * \return True if the path is a directory, false if its a file or does not exist.
         */
        const bool IsDir(const std::string &dirPath) const override;

        /**
         * \brief Copies the original file to the destination path.
         *        No Op (returns true) if both arguments match
         * \param originalFile - the source file
         * \param destinationPath - Where to copy to.
         * \throw FileSystemException if the originalFile does not exist, or the destination path
         *        can not be written to.
         */
        void CopyFile(const std::string &originalFile, const std::string &destinationPath) const override;

        /**
         * \brief copies the original dir, and its contents to the destination path.
         *        No-Op if the two parameters match.
         * \param originalDir - The source directory
         * \param destinationPath - the destination directory
         * \throw FileSystemException if we can't write a file or can't find the original directory.
         * \throw std::invalid_argument if original dir does not exist, or destination dir already exists.
         */
        void CopyDir(const std::string &originalDir, const std::string &destinationPath) const override;

        /**
         * \brief renames the File.  Also works as a Move File
         *        No-Op if the two parameters match.
         * \param originalFile - The original file to move or rename. This is removed.
         * \param newFileName - the new file name or where to move the file to.
         * \throw FileSystemException if something goes wrong.
         */
        void RenameFile(const std::string &originalFile, const std::string &newFileName) const override;

        /**
         * \brief renames the Directory.  Also works as a Move Directory
         *        No-Op if the two parameters match.
         * \param originalDir - The original dir to move or rename. This is removed.
         * \param newDirName - the new dir name or where to move the dir to.
         * \warning It is assumed all path separators in the params are '/', if not change it.
         * \throw FileSystemException if something goes wrong.
         */
        void RenameDir(const std::string &originalDir, const std::string &newDirName) const override;

        /**
         * \brief Deletes the given file
         * \param filePath - the file to delete
         * \throw FileSystemException if the file does not exist or can't be deleted.
         */
        void DeleteFile(const std::string &filePath) const override;

        /**
         * \brief Deletes the given dir and ALL of its contents.
         * \param dirPath - the directory to delete
         * \warning This will delete the directory and all its contents. You have been warned!
         * \throw FileSystemException if the directory, or any of its contents can't be deleted.
         *        if thrown, its possible for some files to be left over.
         */
        void DeleteDir (const std::string &dirPath) const override;

        /**
         * \brief Gets the files in a given directory.
         * \param dirPath - The path to check.
         * \throw FileSystemException if dirPath does not exist.
         */
        const std::vector<std::string> GetFilesInDir(const std::string &dirPath) const override;

        /**
         * \brief Compares the two files.
         *        Returns true if parameters match.
         * \param fileOne - the first file to compare
         * \param fileTwo - the second file to compare
         * \throw FileSystemException if either file does not exist
         * \throw std::invalid_argument if the parameters are not files.
         * \return True if the file contents match exactly, or parameters match
         *         False otherwise.
         */
        const bool CompareFiles (const std::string &fileOne, const std::string &fileTwo) const override;

        /**
         * \brief Compares the two directories.
         *        Returns true if parameters match.
         * \param dirOne - the first file to compare
         * \param dirTwo - the second file to compare
         * \throw FileSystemException if either directory does not exist
         * \throw std::invalid_argument if the parameters are not directories.
         * \return True if the directory contents match exactly, or parameters match.
         *         This means that all subdirectories and files must match the names
         *         and contents.
         *         False otherwise.
         */
        const bool CompareDirs (const std::string &dirOne, const std::string &dirTwo) const override;

	private:
        FileSystem();
        FileSystem(const FileSystem&) = delete;
};

}
#endif
