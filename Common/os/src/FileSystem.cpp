
//          Copyright Seth Hendrick 2014.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file ../LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <algorithm>
#include <cstdio>
#include <errno.h>
#include <fstream>
#include <string>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <vector>

#ifndef MSVC
	#include <dirent.h>
	#include <unistd.h>
#endif

#ifdef WIN32
	#include <windows.h>
#endif

#include "FileSystem.h"

namespace OS {

// -------- File System Exception --------

const std::string FileSystemException::ErrorPrefix = "Error accessing the file/directory: ";

FileSystemException::FileSystemException(const std::string &filePath) :
    m_errorMessage (ErrorPrefix + filePath),
    m_badFilePath (filePath)
{

}

FileSystemException::~FileSystemException()
{

}

const char *FileSystemException::what() const noexcept
{
    return m_errorMessage.c_str();
}

const std::string &FileSystemException::GetBadFilePath() const
{
    return m_badFilePath;
}

// -------- FileSystem ---------

const std::string FileSystem::THIS_DIR = ".";
const std::string FileSystem::UP_DIR = "..";

const char FileSystem::FILE_SEPARATOR = '/';

const FileSystem &FileSystem::GetInstance()
{
    static const FileSystem fs;
    return fs;
}

const std::string FileSystem::PathJoin(const std::string &parent, const std::string &child)
{
	return std::string(parent + FileSystem::FILE_SEPARATOR + child);
}

FileSystem::FileSystem()
{
}

FileSystem::~FileSystem()
{
}

const std::string FileSystem::GetCWD() const
{
    std::string ret;
    #ifdef WIN32
    char dir[MAX_PATH];
	GetModuleFileNameA( NULL, dir, MAX_PATH );
    ret = std::string (dir);
    std::string::size_type pos = ret.find_last_of( "\\/" ); //Get rid of the filename
    ret = ret.substr(0, pos);

    #else

    char dir [PATH_MAX];
    getcwd(dir, PATH_MAX);
    ret = std::string(dir);

    #endif
    return ret;
}

const std::string FileSystem::ReadFile(const std::string &filePath) const
{
    std::ifstream inFile(filePath, std::ios::binary);

    // Ensure the file opened.
    if (inFile.is_open() == false)
    {
        throw FileSystemException(filePath);
    }

    // Read the entire file
    std::ostringstream contents;
    contents << inFile.rdbuf();
    inFile.close();

    // Return the contents.
    return contents.str();
}

void FileSystem::WriteFile(const std::string &stringToWrite, const std::string &filePath) const
{
    std::ofstream outFile(filePath, std::ios::binary);

    // Ensure the file opened.
    if (outFile.is_open() == false)
    {
        throw FileSystemException(filePath);
    }

    outFile << stringToWrite;

    outFile.close();
}

void FileSystem::CreateFile(const std::string &filePath) const
{
    std::ofstream outFile(filePath, std::ios::binary);

    // Ensure the file opened.
    if (outFile.is_open() == false)
    {
        throw FileSystemException(filePath);
    }
    outFile.close();
}

void FileSystem::CreateDir(const std::string &dirPath) const
{
    //If path is blank, throw
    if (dirPath == ""){
        throw std::invalid_argument("CreateDir(): Directory path can not be blank!");
    }

    std::stringstream ss(dirPath);
    std::vector<std::string> dirsToMake;

    char firstChar = ss.peek();

    //If the first character is /, it means we want to create a dir in root.
	if (firstChar == FileSystem::FILE_SEPARATOR){
        std::string nextDir;
		std::getline(ss, nextDir, FileSystem::FILE_SEPARATOR);
        if (ss.str() != ""){                //If empty string, ignore
			nextDir = FileSystem::FILE_SEPARATOR + nextDir;
            dirsToMake.push_back(nextDir);
        }
        ss.peek();
    }
    while (!ss.eof()){
        std::string nextDir;
		std::getline(ss, nextDir, FileSystem::FILE_SEPARATOR);
        dirsToMake.push_back(nextDir);
        ss.peek();
    }

    std::string currentDir = dirsToMake[0];
    for (unsigned int i = 1; i < dirsToMake.size()+1; ++i){
        if (!DirExists(currentDir)){
			#ifdef MSVC
				int status = CreateDirectoryA(currentDir.c_str(), NULL);
				if (status != 0){
					status = 0;
				}
				else{
					status = 1;
				}
			#elif WIN32
				int status = mkdir(currentDir.c_str());
            #else
				mode_t mode = (S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH); //rwxr-xr-x

				int status = mkdir (currentDir.c_str(), mode);
            #endif
            if (status != 0){
                throw FileSystemException(dirPath);
            }
        }
        if (i < dirsToMake.size()){
            currentDir = PathJoin(currentDir, dirsToMake[i]);
        }
    }
}

const bool FileSystem::FileExists(const std::string &filePath) const
{
    std::ifstream inFile(filePath, std::ios::binary);
    bool exists = inFile.is_open();
    inFile.close();

    return exists;
}

const bool FileSystem::DirExists(const std::string &dirPath) const
{
    bool ret = false;

	#ifdef MSVC
	    int status = GetFileAttributesA(dirPath.c_str());
		if (status == FILE_ATTRIBUTE_DIRECTORY){
			ret = true;
		}
	#else
		DIR *dir;
		dir = opendir(dirPath.c_str());
		if (dir == nullptr){
			ret = false;
		}
		else{
			ret = true;
		}
		if (dir != nullptr){
			closedir(dir);
		}

	#endif
    return ret;
}

const bool FileSystem::IsFile(const std::string &filePath) const
{
    bool isFile;

    struct stat s;

    if( stat(filePath.c_str(),&s) == 0 )
    {
        if( s.st_mode & S_IFREG )
        {
            //it's a file
            isFile = true;
        }
        else
        {
            isFile = false;
        }
    }
    else
    {
        // File does not exist, throw.
        throw FileSystemException(filePath);
    }
    return isFile;
}

const bool FileSystem::IsDir(const std::string &dirPath) const
{
    bool isDir;

    struct stat s;

    if( stat(dirPath.c_str(),&s) == 0 )
    {
        if( s.st_mode & S_IFDIR )
        {
            //it's a directory
            isDir = true;
        }
        else
        {
            isDir = false;
        }
    }
    else
    {
        // Dir does not exist, throw.
        throw FileSystemException(dirPath);
    }
    return isDir;
}

void FileSystem::CopyFile(const std::string &originalFile, const std::string &destinationPath) const
{
    if (originalFile == destinationPath)
    {
        return; //No op
    }

    //If infile fails, Throw before we do anything to the outfile.
    std::ifstream inFile(originalFile, std::ios::binary);
    if (inFile.is_open() == false)
    {
        throw FileSystemException(originalFile);
    }

    std::ofstream outFile(destinationPath, std::ios::binary);
    if (outFile.is_open() == false)
    {
        throw FileSystemException(destinationPath);
    }

    outFile << inFile.rdbuf();

    // Ensure the copy worked.
    if (inFile.fail())
    {
        throw FileSystemException(originalFile);
    }
    else if (outFile.fail())
    {
        throw FileSystemException(destinationPath);
    }
}

void FileSystem::CopyDir (const std::string &originalDir, const std::string &destinationPath) const
{
    //Ensure the originalDir actually exists, and the destination does not already exist
    if (DirExists(originalDir) == false)
    {
        throw std::invalid_argument(originalDir + " does not exist");
    }
    else if (DirExists(destinationPath))
    {
        throw std::invalid_argument(destinationPath + " already exists!");
    }

    //Make destination dir
    CreateDir(destinationPath);

    //Get files listed in originalDir
    const std::vector<std::string> files = GetFilesInDir(originalDir);

    for (size_t i = 0, size = files.size(); i < size; ++i)
    {
        //Ignore .. and .
        if ((files[i] != UP_DIR) && (files[i] != THIS_DIR))
        {
            const std::string originalPath = PathJoin(originalDir, files[i]);
            const std::string newPath = PathJoin(destinationPath, files[i]);

            if (IsFile(originalPath))
            {
                CopyFile(originalPath, newPath);
            }
            else if (IsDir(originalPath))
            {
                //Create the new dir
                CreateDir(newPath);
                CopyDir(originalPath, newPath);
            }
            //Else ignore
        }
    }
}

void FileSystem::RenameFile(const std::string &originalFile, const std::string &newFileName) const
{
    if (originalFile == newFileName)
    {
        return; //No op
    }
    else
    {
        int status = ::rename(originalFile.c_str(), newFileName.c_str());
        if (status != 0)
        {
            throw FileSystemException(originalFile);
        }
    }
}

void FileSystem::RenameDir(const std::string &originalDir, const std::string &newDirName) const
{
    RenameFile(originalDir, newDirName);
}

void FileSystem::DeleteFile(const std::string &filePath) const
{
    int status = ::remove(filePath.c_str());
    if (status != 0)
    {
        throw FileSystemException(filePath);
    }
}

void FileSystem::DeleteDir (const std::string &dirPath) const
{
    const std::vector<std::string> files = GetFilesInDir(dirPath);

    for (size_t i = 0, size = files.size(); i < size; ++i){
        //Ignore .  and ..
        if ((files[i] != UP_DIR) && (files[i] != THIS_DIR))
        {
            std::string path = PathJoin(dirPath, files[i]);

            if (IsFile(path))
            {
                DeleteFile(path);
            }
            else if (IsDir(path))
            {
                DeleteDir(path);
            }
            //Else ignore
        }
    }

    //Finally, delete the root dir
    #ifdef MSVC
        int status = RemoveDirectoryA(dirPath.c_str());
        bool success = (status != 0);
    #else
        int status = rmdir(dirPath.c_str());
        bool success = (status == 0);
    #endif

    if (success == false)
    {
        throw FileSystemException(dirPath);
    }
}

const std::vector<std::string> FileSystem::GetFilesInDir(const std::string &dirPath) const
{
    std::vector<std::string> fileNamesInDir;

	#ifdef MSVC
		HANDLE hFind;
		WIN32_FIND_DATAA data;

		std::string glob = dirPath + "/*.*";
		hFind = FindFirstFileA(glob.c_str(), &data);
		if (hFind == INVALID_HANDLE_VALUE)
        {
			throw FileSystemException(dirPath);
		}
		else
        {
			do
            {
				fileNamesInDir.push_back(std::string(data.cFileName));
			}
			while (FindNextFileA(hFind, &data));
			FindClose(hFind);
		}
	#else
		DIR *dir;
		dir = opendir(dirPath.c_str());
		if (dir == nullptr)
        {
			throw FileSystemException(dirPath);
		}
		else
		{
			dirent *de = readdir(dir);
			while (de != nullptr)
			{
				fileNamesInDir.push_back(de->d_name);
				de = readdir(dir);
			}
			closedir(dir);
		}
	#endif

	return fileNamesInDir;
}

const bool FileSystem::CompareFiles(const std::string &fileOne, const std::string &fileTwo) const
{
    //Ensure both are files
    if (IsFile(fileOne) == false)
    {
        throw std::invalid_argument(fileOne + " is not a file");
    }
    else if (IsFile(fileTwo) == false)
    {
        throw std::invalid_argument(fileTwo + " is not a file");
    }
    else if (fileOne == fileTwo)
    {
        return true; //No op
    }

    std::ifstream inFile1 (fileOne, std::ios::binary);
    std::ifstream inFile2 (fileTwo, std::ios::binary);

    if (inFile1.is_open() == false)
    {
        throw FileSystemException(fileOne);
    }
    else if (inFile2.is_open() == false)
    {
        throw FileSystemException(fileTwo);
    }

    // First check, ensure the sizes are the same, if not we can stop.
    inFile1.seekg(0, std::ios::end);
    int size1 = inFile1.tellg();

    inFile2.seekg(0, std::ios::end);
    int size2 = inFile2.tellg();

    if (size1 != size2)
    {
        return false;
    }
    // If they are both empty, return now!
    else if (( size1 == 0) && (size2 == 0))
    {
        return true;
    }

    // Otherwise, we need to traverse through the entire file.
    // Reset the seekers.
    inFile1.seekg(0, std::ios::beg);
    inFile2.seekg(0, std::ios::beg);

    do
    {
        // If characters don't match, stop and return false.
        if (inFile1.get() != inFile2.get())
        {
            return false;
        }
    }
    while ((inFile1.eof() == false) && (inFile2.eof() == false));

    return true;
}

const bool FileSystem::CompareDirs(const std::string &dirOne, const std::string &dirTwo) const
{
    //Ensure both are dirs
    if (IsDir(dirOne) == false)
    {
        throw std::invalid_argument(dirOne + " is not a directory");
    }
    else if (IsDir(dirTwo) == false)
    {
        throw std::invalid_argument(dirTwo + " is not a directory");
    }
    //If both are the same name, they must be the same dir
    else if (dirOne == dirTwo)
    {
        return true;
    }

    std::vector <std::string> dir1Contents = GetFilesInDir(dirOne);
    std::vector <std::string> dir2Contents = GetFilesInDir(dirTwo);

    bool status = true;

    //Ensure the sizes are the same
    if (dir1Contents.size() == dir2Contents.size())
    {
        //Sort both
        std::sort (dir1Contents.begin(), dir1Contents.end());
        std::sort (dir2Contents.begin(), dir2Contents.end());

        for (size_t i = 0, size = dir1Contents.size(); (i < size) && status; ++i)
        {
            std::string path1 = PathJoin(dirOne, dir1Contents[i]);
            std::string path2 = PathJoin(dirTwo, dir2Contents[i]);

            // Ensure the names match, if not return false.
            if (dir1Contents[i] != dir2Contents[i])
            {
                status = false;
            }
            //If both are directories
            else if (IsDir(path1) && IsDir(path2))
            {
                //Only compare dirs ifthey are not "." or ".."
                if ((dir1Contents[i] != UP_DIR) && (dir1Contents[i] != THIS_DIR))
                {
                    status = CompareDirs(PathJoin(dirOne, dir1Contents[i]), PathJoin(dirTwo, dir2Contents[i]));
                }
            }
            //If both are files
            else if (IsFile(path1) && IsFile(path2))
            {
                if ((dir1Contents[i] != UP_DIR) && (dir1Contents[i] != THIS_DIR))
                {
                    status = CompareFiles(PathJoin(dirOne, dir1Contents[i]), PathJoin(dirTwo, dir2Contents[i]));
                }
            }
            //The file types do not match
            else
            {
                status = false;
            }
        }
    }
    else
    {
        status = false;
    }

    return status;
}

}
